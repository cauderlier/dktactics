-include .config_vars

INSTALL_BIN_DIR=/usr/local/bin
INSTALL_LIB_DIR=/usr/local/lib/dedukti/dktactics

SUBDIRS= fol meta prettyprint ugly example

# To activate confluence checking, set this to "--with-confluence-checking"
CONFIGURE_FLAGS=

all:
	for d in $(SUBDIRS); do \
		$(MAKE) all -C $$d; \
	done

clean:
	for d in $(SUBDIRS); do\
		$(MAKE) clean -C $$d; \
	done
	rm -f dktactics

dktactics:
	echo "#!/bin/sh" > $@
	echo "echo \"$(INSTALL_LIB_DIR)\"" >> $@

install: all dktactics
	mkdir -p $(INSTALL_LIB_DIR)
	for d in $(SUBDIRS); do \
		$(MAKE) INSTALL_LIB_DIR=$(INSTALL_LIB_DIR)/$$d install -C $$d; \
	done
	mkdir -p $(INSTALL_BIN_DIR)
	install -t $(INSTALL_BIN_DIR) dktactics

uninstall:
	for d in $(SUBDIRS); do \
		$(MAKE) INSTALL_LIB_DIR=$(INSTALL_LIB_DIR)/$$d uninstall -C $$d; \
	done
	rm -f $(INSTALL_BIN_DIR)/dktactics
	rmdir $(INSTALL_LIB_DIR)

.config_vars: configure
	./configure $(CONFIGURE_FLAGS)

.PHONY:	clean install uninstall all
