#NAME printf.

digit : Type.
d0 : digit.
d1 : digit.
d2 : digit.
d3 : digit.
d4 : digit.
d5 : digit.
d6 : digit.
d7 : digit.
d8 : digit.
d9 : digit.

(; Number in decimal notation ;)
dlist : Type.
dnil : dlist.
dsnoc : dlist -> digit -> dlist.

def dincr : dlist -> dlist.
[] dincr dnil --> dsnoc dnil d1
[l] dincr (dsnoc l d0) --> dsnoc l d1
[l] dincr (dsnoc l d1) --> dsnoc l d2
[l] dincr (dsnoc l d2) --> dsnoc l d3
[l] dincr (dsnoc l d3) --> dsnoc l d4
[l] dincr (dsnoc l d4) --> dsnoc l d5
[l] dincr (dsnoc l d5) --> dsnoc l d6
[l] dincr (dsnoc l d6) --> dsnoc l d7
[l] dincr (dsnoc l d7) --> dsnoc l d8
[l] dincr (dsnoc l d8) --> dsnoc l d9
[l] dincr (dsnoc l d9) --> dsnoc (dincr l) d0.

def nat_to_dlist : nat -> dlist.
[] nat_to_dlist 0 --> dnil
[n] nat_to_dlist (S n) --> dincr (nat_to_dlist n).

def char_to_string (c : char) : string := string_cons c "".
def string_snoc (s : string) (c : char) := string.append s (char_to_string c).

def digit_to_char : digit -> char.
[] digit_to_char d0 --> '0'
[] digit_to_char d1 --> '1'
[] digit_to_char d2 --> '2'
[] digit_to_char d3 --> '3'
[] digit_to_char d4 --> '4'
[] digit_to_char d5 --> '5'
[] digit_to_char d6 --> '6'
[] digit_to_char d7 --> '7'
[] digit_to_char d8 --> '8'
[] digit_to_char d9 --> '9'.

def dlist_to_string : dlist -> string.
def dlist_to_string_aux : dlist -> string.
[] dlist_to_string_aux dnil --> ""
[l,d] dlist_to_string_aux (dsnoc l d) -->
  string_snoc (dlist_to_string_aux l) (digit_to_char d).
[] dlist_to_string dnil --> "0"
[l,d] dlist_to_string (dsnoc l d) -->
  string_snoc (dlist_to_string_aux l) (digit_to_char d).

def nat_to_string (n : nat) : string :=
  dlist_to_string (nat_to_dlist n).

def printf_type : string -> Type.
[] printf_type "" --> string
[s] printf_type (string_cons '%' (string_cons 'd' s)) --> nat -> printf_type s
[s] printf_type (string_cons '%' (string_cons 's' s)) --> string -> printf_type s
[s] printf_type (string_cons _ s) --> printf_type s.

def printf_acc : fmt : string -> string -> printf_type fmt.
[acc] printf_acc "" acc --> acc
[fmt,acc] printf_acc (string_cons '%' (string_cons 'd' fmt)) acc --> n : nat =>
  printf_acc fmt (string.append acc (nat_to_string n))
[fmt,acc] printf_acc (string_cons '%' (string_cons 's' fmt)) acc --> str : string =>
  printf_acc fmt (string.append acc str)
[c,fmt,acc] printf_acc (string_cons c fmt) acc -->
  printf_acc fmt (string_snoc acc c).

def printf (fmt : string) : printf_type fmt := printf_acc fmt "".
