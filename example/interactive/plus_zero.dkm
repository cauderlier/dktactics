#NAME plus_zero.

(; This example illustrates Deduction Modulo  ;)

(; We start by an encoding of the following theory of Peano addition:

NAT : sort.
o : NAT.
succ : NAT -> NAT.

plus : NAT -> NAT -> NAT.
[n] plus o n --> n
[m,n] plus (succ m) n --> succ (plus m n)

induction_axiom (P : NAT -> prop) :
  P o -> (n : NAT -> P n -> P (succ n)) -> n : NAT -> P n.
 ;)

NAT : fol.sort.

O : fol.function.
[] fol.fun_domain O --> fol.nil_sort.
[] fol.fun_codomain O --> NAT.

def o : fol.term NAT := fol.fun_apply O.

SUCC : fol.function.
[] fol.fun_domain SUCC --> fol.read_sorts 1 NAT.
[] fol.fun_codomain SUCC --> NAT.

def succ (n : fol.term NAT) : fol.term NAT := fol.fun_apply SUCC n.

PLUS : fol.function.
[] fol.fun_domain PLUS --> fol.read_sorts 2 NAT NAT.
[] fol.fun_codomain PLUS --> NAT.

def plus (a : fol.term NAT) (b : fol.term NAT) := fol.fun_apply PLUS a b.

(; plus 0 n --> n ;)
[n] fol.apply_fun PLUS
      (fol.cons_term _ (fol.apply_fun O fol.nil_term) _
      (fol.cons_term _ n _ fol.nil_term)) -->
  n
(; plus (succ m) n --> succ (plus m n) ;)
[m,n] fol.apply_fun PLUS
      (fol.cons_term _ (fol.apply_fun SUCC
                           (fol.cons_term _ m _ fol.nil_term)) _
      (fol.cons_term _ n _ fol.nil_term)) -->
  succ (plus m n).

def induction_statement (P : fol.term NAT -> fol.prop) : fol.prop :=
  fol.imp (P o) (
  fol.imp (fol.all NAT (n => fol.imp (P n) (P (succ n)))) (
  fol.all NAT P)).

induction_axiom :
  P : (fol.term NAT -> fol.prop) ->
  fol.proof (induction_statement P).

(; Custom Tactics ;)

(; Tactic matching formulae of the form (fol.all NAT P) ;)
def match_all_NAT (a : fol.prop)
                  (t0 : (fol.term NAT -> fol.prop) -> tactic.tactic)
                  (t1 : tactic.tactic) : tactic.tactic :=
  tactic.match_all a (A => P => tactic.ifeq_sort NAT A (NAT2A =>
     t0 (x => P (NAT2A x))) t1) t1.

induction_failure : mtac.exc.

(; induction_tactic t0 t1 proves G |- ∀n:NAT, P n if t0 proves G |- P 0 and
   t1 proves G |- ∀n:NAT, P n → P (succ n) ;)
def induction_tactic (t0 : tactic.tactic) (tS : tactic.tactic) : tactic.tactic :=
  tactic.with_goal (G =>
  match_all_NAT G (P =>
    tactic.refine2 (P o) (fol.all NAT (n => fol.imp (P n) (P (succ n))))
      (fol.all NAT P)
      (induction_axiom P) t0 tS)
    (tactic.raise induction_failure)).

(; Pretty-printing ;)

load_pretty : pretty.pretty_load.

[] pp.pp_sort NAT --> "NAT".
[] pp.pp_fun PLUS --> "PLUS".
[] pp.pp_fun O --> "O".
[] pp.pp_fun SUCC --> "SUCC".

(; Interactive Proof ;)
def t : nat -> tactic.tactic.

[] t 0 --> induction_tactic
     eqtac.reflexivity
     (tactic.lintro "n" NAT (n =>
      tactic.intro (
      eqtac.f_equal SUCC
        tactic.assumption))).


def plus_zero_statement : fol.prop := fol.all NAT (n => eq.eq NAT (plus n o) n).

def plus_zero : fol.proof plus_zero_statement :=
  tactic.prove plus_zero_statement (t 0).

