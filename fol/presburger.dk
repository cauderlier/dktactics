#NAME presburger.

(; The first-order theory of Presburger arithmetic:
https://en.wikipedia.org/wiki/Presburger_arithmetic ;)


(; There is just one sort representing natural numbers ;)
Nat : fol.sort.

(; There is just one predicate symbol for equality ;)
EQ : fol.predicate.

(; Function symbols:
- 0 : Nat.
- 1 : Nat.
- + : [Nat, Nat] -> Nat.
 ;)

ZERO : fol.function.
ONE : fol.function.
ADD : fol.function.

(; Arities ;)
[] fol.pred_domain EQ --> fol.read_sorts 2 Nat Nat.
[] fol.fun_domain ZERO --> fol.nil_sort.
[] fol.fun_domain ONE --> fol.nil_sort.
[] fol.fun_domain ADD --> fol.read_sorts 2 Nat Nat.
[] fol.fun_codomain ZERO --> Nat.
[] fol.fun_codomain ONE --> Nat.
[] fol.fun_codomain ADD --> Nat.

(; Abbreviations ;)
def eq := fol.pred_apply EQ.
def 0_ := fol.fun_apply ZERO.
def 1_ := fol.fun_apply ONE.
def add := fol.fun_apply ADD.

def succ (x : fol.term Nat) := add x 1_.
(; Axioms ;)

(; 0 != x + 1 ;)
P1 : fol.proof (fol.all Nat (x => fol.not (eq 0_ (succ x)))).

(; x+1 = y+1 -> x = y ;)
P2 : fol.proof (fol.all Nat (x => fol.all Nat (y => fol.imp (eq (succ x) (succ y)) (eq x y)))).

(; x+0 = x ;)
P3 : fol.proof (fol.all Nat (x => eq (add x 0_) x)).

(; x + (y + 1) = (x + y) + 1 ;)
P4 : fol.proof (fol.all Nat (x => fol.all Nat (y => eq (add x (succ y)) (succ (add x y))))).

(; Axiom schema of induction ;)
P5 : p : (fol.term Nat -> fol.prop) ->
     fol.proof (fol.imp (fol.and (p 0_) (fol.all Nat (x => fol.imp (p x) (p (succ x))))) (fol.all Nat p)).